First Microserives project - Blog with Posts and Comments on Posts

Tools used:

1. Docker
2. Kubernetes
3. Minikube
4. NGINX Ingress Controller
5. VirtualBox - required for ingress load balancer
6. Skaffold - watches for changes in Kubernetes config yaml files

---

Kubernetes notes(Minikube Users):

!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
minikube start (currently driver is set to VirtualBox) (or [minikube start --driver=virtualbox])
minikube addons enable ingress - run this when a minikube is deleted and created anew
!!! eval $(minikube docker-env) - not sure is this line is still needed when building with docker
!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

If you are using a vm driver, you will need to tell Kubernetes to use the Docker daemon running inside of the single node cluster instead of the host.

Run the following command:
eval $(minikube docker-env)
Note - This command will need to be repeated anytime you close and restart the terminal session.

Afterward, you can build your image:
docker build -t USERNAME/REPO .

Update, your pod manifest as shown above and then run:
kubectl apply -f infra/k8s/

https://minikube.sigs.k8s.io/docs/commands/docker-env/

---

Minikube commands (Minikube - is a local Kubernetes runner):

1. minikube start - run it after machine restart to start eh minikube.
2. eval $(minikube docker-env) - run it after machine restart
3. minikube ip - prints out the address from which we can access the running containers
4. minikube addons enable ingress - to enable ingress-nginx
5. minikube profile list - display which driver minikube is using.
   These files contain minikube configs:
   ~/.minikube/profiles/minikube/config.json
   ~/.minikube/machines/minikube/config.json
6. minikube config view - will display something if "set" was ever used, oterwise won't show anything
7. minikube delete - to delete minikube cluster
8. minikube start --memory '8g' - by default it's 2GB, and it wasn't enought to run npm install on a client pod.

Docker commands:

3. docker ps (docker ps --all) - List all containers
4. docker build -t de1ain/servicename:0.0.1 .
5. docker create [container_name]
6. docker start -a de1ain/servicename
   docker start -a [container_id]
7. docker run -it de1ain/servicename
   docker run [container_id]
8. docker exec -it [running_container_id] sh
9. docker logs [container_id]

Kubernetes Pods commands:

1. kubectl get pods - print info about all the running pods
2. kubectl exec -it [podname] [cmd] - executes the given command in a running pod
   Example: kubectl exec -it posts sh
3. kubectl logs [podname] - prints out logs for the given pod
   Example: kubectl logs posts
4. kubectl delete pod [podname] - deletes the given pod
   Example: kubectl delete posts
5. kubectl apply -f [config_file_name] - tells kubernetes to process the config
   Example: kubectl apply -f posts.yaml
6. kubectl describe pod [podname] - print some info about the ruinning pod
   Example: kubectl describe pod posts

Kubernetes Deployments commands:

1. kubectl get deployments
2. kubectl describe deployment [depl_name] - Print out details about a specific deployment
3. k apply -f [config_file_name] - creates a deployment out of a config file
4. k delete deployment [depl_name]
5. k rollout restart deployment [depl_name]

Updating the image used by a Deployment:

1. The deployment must use the 'latest' tag in the pod spec section
2. Make an update to your code
3. Build the image
4. Push the image to docker hub
5. Run the command: kubectl rollout restart deployment [depl_name]

Kubernetes Services commands:

1. kubectl get services
2. k describe service posts-srv - prints out data about the Service

---

The entire process in order to access your service in browser:
(For development purposes we used Service of kind NodePort)

1. Create your service (write code)
2. Build a docker image - "docker build -t de1ain/posts ." Don't mention the version number, by default it's latest
3. Push to docker hub - "docker push de1ain/[container-name]"
4. Create a Deployment - posts-depl.yaml and run "kubectl apply -f posts-depl.yaml"
5. Create a Service (of type NodePort) - posts-srv.yaml and run "kubectl apply -f posts-srv.yaml"
6. Make sure to apply Service, and check on which port it runs, or run "kubectl describe service posts-srv" or "k get services"
7. Get minikube ip - run command "minikube ip"
8. The url should look like this: http://192.168.99.101:32146/posts
   The ip changes after machine restart, get the new it by running "minikube ip"
9. Client requests are sent to posts.com. posts.com is defined in hosts file (etc/hosts) as follows: "192.168.99.101 posts.com" (the ip is minikube's ip)

---

Adding more services:

1. For "comments", "query", moderation"...
2. Update the URL's in each to reach out ot he "event-bus-srv"
3. Build image + push to docker hub
4. Create a deployment + clusterip service for each
5. Update the event-bus to once again send events to "comments", "query" and moderation"

---

Skaffold commands:

skaffold dev

---

Notes:

1. In "ingress-srv.yaml" file, there's a line: "- path: /". For our super simple client side it is ok. But for a more complex use regex: "/?(.\*)".
   This line has to be at the end.
2. The project also uses Skaffold - Skaffold is a command line tool that facilitates continuous development for Kubernetes-native applications.
